require_relative 'board.rb'

class ComputerPlayer
  attr_accessor :mark, :board

  def initialize(name)
    @name=name
  end

  def display(board)
    @board=board
  end

  def random_move
    ran_row=rand(2)
    ran_col=rand(2)
    [ran_row,ran_col]
  end

  def winning_move?
    @board.grid.each_with_index do |row,row_idx|
      row.each_index do |col_idx|
        pos = [row_idx,col_idx]
        next if !@board.empty?(pos)
        @board.place_mark(pos, @mark)
        if @board.winner
          return pos
        else
          nilify_cell(pos)
        end
      end
    end
    false
  end

  def nilify_cell(pos)
    @board.grid[pos[0]][pos[1]] = nil
  end

  def get_move
    winning_move = winning_move?
    winning_move ? winning_move : random_move
  end

end
