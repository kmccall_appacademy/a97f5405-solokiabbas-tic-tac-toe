class Board
  attr_accessor :players, :victor
  attr_reader :winner, :grid

  def initialize(grid=default_grid)
    @grid=grid
    @victor=nil
  end

  def default_grid()
    Array.new(3){Array.new(3)}
  end

  def place_mark(pos, mark)
    row=pos[0].to_i
    col=pos[1].to_i
    if empty?(pos)
      @grid[row][col]=mark
    else
      puts "Invalid placement!"
    end
  end

  def empty?(pos)
    row=pos[0]
    col=pos[1]
    @grid[row][col]==nil
  end

  def[](pos)
    row,col=pos
    @grid[row][col]
  end

  def []=(pos,mark)
    row,col=pos
    @grid[row][col]=mark
  end

  def winner
    win_row?
    win_col?
    win_diag?
    @victor
  end

  def over?
    (winner != nil) || tied?
  end

  def tied?
    no_nil_cells = 0
    @grid.each do |row|
      no_nil_cells += 1 if !row.all?(&:nil?)
    end
    no_nil_cells == 3
  end

  def win_row?
    @grid.each do |row|
      if row.all? { |cell| cell == :X }
        @victor = :X
      elsif row.all? { |cell| cell == :O }
        @victor = :O
      end
    end
  end

  def win_col?
    @grid.transpose.each do |col|
      if col.all? { |cell| cell == :X }
        @victor = :X
      elsif col.all? { |cell| cell == :O }
        @victor = :O
      end
    end
  end
  
  def get_cell(pos)
    @grid[pos[0]][pos[1]]
  end

    def diagonals
      [
        [get_cell([0, 0]), get_cell([1, 1]), get_cell([2, 2])],
        [get_cell([0, 2]), get_cell([1, 1]), get_cell([2, 0])]
      ]
    end

  def win_diag?
    diagonals.each do |row|
      if row.all? { |cell| cell == :X }
        @victor = :X
      elsif row.all? { |cell| cell == :O }
        @victor = :O
      end
    end
  end

end
